# Copyright 2023 Timothy Chandler
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import random
from .card import Card


class Deck:
    def __init__(self, cards=None):
        if cards is not None:
            self.cards = cards
        else:
            self.cards = [
                Card(rank, suit) for suit in Card.suits for rank in Card.ranks
            ]

    def __str__(self):
        return self.cards

    def add(self, card):
        self.cards.append(card)

    def draw_from_top(self):
        return self.cards.pop()

    def cut(self):
        self.cards = (
            self.cards[(len(self.cards) // 2):] + self.cards[: (len(self.cards) // 2)]
        )

    def shuffle(self):
        cut_margin = len(self.cards) // 10
        cut_index = len(self.cards) // 2 + random.randint(-cut_margin, cut_margin)
        left_stack = self.cards[:cut_index]
        right_stack = self.cards[cut_index:]

        shuffled_cards = []
        while left_stack or right_stack:
            left_num_cards = (
                random.randint(1, min(len(left_stack), 3)) if left_stack else 0
            )
            right_num_cards = (
                random.randint(1, min(len(right_stack), 3)) if right_stack else 0
            )
            shuffled_cards += (
                right_stack[:right_num_cards] + left_stack[:left_num_cards]
            )
            left_stack = left_stack[left_num_cards:]
            right_stack = right_stack[right_num_cards:]
        self.cards = shuffled_cards
