# Copyright 2023 Timothy Chandler
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class Card:
    suits = ("Spades", "Hearts", "Clubs", "Diamonds")
    ranks = ("A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K")

    def __init__(self, rank, suit):
        if suit in Card.suits:
            self.suit = suit
        else:
            raise ValueError("Invalid suit: {}".format(suit))
        if rank in Card.ranks:
            self.rank = rank
        else:
            raise ValueError("Invalid rank: {}".format(rank))

    def __repr__(self):
        return f"{self.rank} of {self.suit}"
