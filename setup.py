from setuptools import setup, find_packages

package_name = 'card_game'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(include=["card_game", "card_game.*"]),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Timothy Chandler',
    maintainer_email='ernaldis+git@pm.me',
    description='An example application that models a card game',
    license='Apache-2.0',
    tests_require=['unittest'],
    entry_points={
        'console_scripts': [
            'dealer = card_game.game.dealer:main',
            'player = card_game.game.player:main',
        ],
    },
)
