# Copyright 2023 Timothy Chandler
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
from card_game.cards.deck import Deck, Card


class TestDeck(unittest.TestCase):
    def setUp(self):
        self.deck = Deck()

    def test_canary(self):
        self.assertTrue(True)

    def test_add_first(self):
        new_deck = Deck([])
        new_deck.add(Card("3", "Clubs"))
        self.assertEqual(len(new_deck.cards), 1)

    def test_add(self):
        original_size = len(self.deck.cards)
        card = Card("2", "Hearts")
        self.deck.add(card)
        self.assertEqual(len(self.deck.cards), original_size + 1)

    def test_draw_from_top(self):
        original_size = len(self.deck.cards)
        self.deck.draw_from_top()
        self.assertEqual(len(self.deck.cards), original_size - 1)

    def test_cut(self):
        half_length = len(self.deck.cards) // 2
        original_order = self.deck.cards
        self.deck.cut()
        self.assertEqual(self.deck.cards[:half_length], original_order[half_length:])
        self.assertEqual(self.deck.cards[half_length:], original_order[:half_length])

    def test_shuffle_front(self):
        original_order = self.deck.cards[:]
        self.deck.shuffle()
        self.assertTrue(
            original_order[0] in self.deck.cards[: (len(self.deck.cards) // 2)]
        )

    def test_shuffle_rear(self):
        original_order = self.deck.cards
        self.deck.shuffle()
        self.assertTrue(
            original_order[-1] in self.deck.cards[-(len(self.deck.cards) // 2):]
        )


if __name__ == "__main__":
    unittest.main()
