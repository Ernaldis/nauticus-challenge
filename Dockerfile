FROM ros:galactic

# Update and install dependencies
RUN apt-get update && apt-get install -y \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN echo "source /opt/ros/galactic/setup.bash" >> ~/.bashrc

COPY . /tmp

WORKDIR /tmp

RUN /bin/bash -c 'source /opt/ros/galactic/setup.bash && \
    colcon build && \
    echo "source /tmp/install/setup.bash" >> ~/.bashrc'

